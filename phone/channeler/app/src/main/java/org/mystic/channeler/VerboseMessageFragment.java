package org.mystic.channeler;

import android.os.Bundle;

import androidx.fragment.app.Fragment;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

/**
 * A simple {@link Fragment} subclass.
 * Use the {@link VerboseMessageFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class VerboseMessageFragment extends Fragment {


    public VerboseMessageFragment() {
        // Required empty public constructor
    }

    /**
     * Use this factory method to create a new instance of
     * this fragment using the provided parameters.
     * @return A new instance of fragment VerboseMessageFragment.
     */
    // TODO: Get the messages that are published to the viewmodel and append it to the text box
    public static VerboseMessageFragment newInstance(String param1, String param2) {
        VerboseMessageFragment fragment = new VerboseMessageFragment();
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        return inflater.inflate(R.layout.fragment_verbose_message, container, false);
    }
}