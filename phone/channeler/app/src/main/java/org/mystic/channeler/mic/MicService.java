package org.mystic.channeler.mic;

import android.app.Notification;
import android.app.PendingIntent;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Build;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.util.Log;
import android.widget.Toast;

import org.freedesktop.gstreamer.GStreamer;
import org.mystic.channeler.ConnectionDetailsFragment;
import org.mystic.channeler.MainActivity;
import org.mystic.channeler.R;
import org.mystic.channeler.speaker.SpeakerService;

import java.net.MalformedURLException;
import java.net.URL;

import androidx.annotation.RequiresApi;
import androidx.lifecycle.MutableLiveData;

/*
This is the service that communicates with GStreamer using JNI to achieve Mic
functionality. It sends status updates to the MicFragment and status messages to
StatusMessageFragment.
 */


public class MicService extends Service {
    // TODO: BUG <=> If a GStreamer error occurs then this service shouldn't start.
    // TODO: BUG <=> Starting and stopping service multiple times casues it to not work.

    // TODO: More than 99% of mic and mic service are the same, so make them a single multi-threaded service
    private static final int NOTIFICATION_ID = 2;
    public static boolean micServiceRunning = false;
    private String IP;

    // native methods
    protected native void nativeInit();
    protected static native boolean nativeClassInit();
    protected native void nativeFinalize();
    protected native void nativePlay();
    protected native void nativePause();

    protected String nativePipelineString;
    protected long native_custom_data;

    protected static boolean Initialized = false;
    protected boolean is_playing = false;
    public static MutableLiveData<MicStatus> STATUS;
    public static enum MicStatus {
        NONE, PAUSE, PLAYING, ERROR
    };
    public static MutableLiveData<MicStatus> getCurrentStatus() {
        if (STATUS == null) {
            STATUS = new MutableLiveData<MicStatus>();
        }
        return STATUS;
    }



    public MicService() { }

    public int init(Context serviceContext){
        //Initialize GStreamer and warn if it fails
        try {
            GStreamer.init(serviceContext);
        } catch (Exception e) {
            return 1;
        }
        nativeClassInit();
        nativeInit();
        Initialized = true;
        return 0;
    }

    public boolean play(){
        nativePlay();
        is_playing = true;
        return true;
    }

    protected void onGStreamerInitialized(){

    }

    public boolean pause(){
        nativePause();
        is_playing = false;
        return true;
    }

    public void finalize(){
        Initialized = false;
        nativeFinalize();
    }

    @Override
    public void onCreate(){
        SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(this);
        IP = prefs.getString("pc_ip","");
        //TODO:Ping the ip

        String port = prefs.getString("mic_port","5001");
        Log.d("io-share","port is "+port);
        nativePipelineString = "openslessrc ! audioconvert  ! audioresample ! opusenc ! rtpopuspay ! udpsink host="+IP+" port="+port;
        init(this);
        getCurrentStatus().setValue(MicStatus.NONE);
    }

    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        Intent notificationIntent = new Intent(this, MainActivity.class);
        PendingIntent pendingIntent =
                PendingIntent.getActivity(this, 0, notificationIntent, 0);
        // TODO: Put a pause/stop button action on the notification
        Log.d("Mic","Making the notification");
        Notification notification =
                new Notification.Builder(this, "channeler-notify")
                        .setContentTitle(getText(R.string.notification_title))
                        .setContentText(getText(R.string.notification_mic_message))
                        .setSmallIcon(R.drawable.ic_mic)
                        .setContentIntent(pendingIntent)
                        .setTicker(getText(R.string.ticker_text))
                        .build();

        startForeground(NOTIFICATION_ID, notification);
        if(isPlaying()) pause();
        micServiceRunning = true;
        try {
            // wait till the native code initializes the pipeline
            Thread.sleep(50); // This is a very bad idea
            // TODO : Make pipeline variable thread-safe instead of hardcoding wait time

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        play();

        Toast.makeText(this, "Mic service starting", Toast.LENGTH_SHORT).show();
        return START_STICKY;
    }

    public static boolean isInitialized(){
        return Initialized;
    }

    public boolean isPlaying(){
        return is_playing;
    }


    protected void setMessage(String Message){
        // Send status broadcast to the MicFragment
        // Send status message broadcast to the MessageFragment
        // Everything this method does has to be done should be inside a try catch
        // otherwise the NativeMethod might throw an Error
        try {
            Log.d("Mic", "The message we see here is " + Message);
            if (Message.contains("PAUSED")) getCurrentStatus().postValue(MicStatus.PAUSE);
            else if (Message.contains("PLAYING")) getCurrentStatus().postValue(MicStatus.PLAYING);
            if (Message.contains("ERROR") || (Message.contains("Error")) || (Message.contains("Unable to build") || (Message.contains("Unable to change"))))
                getCurrentStatus().postValue(MicStatus.ERROR);
        } catch (Exception e) {
            Log.e("Mic","Error caught while setMessage()"+e.toString());
        }
    }

    @Override
    public IBinder onBind(Intent intent) {
        // TODO: Return the communication channel to the service.
        throw new UnsupportedOperationException("Not yet implemented");
    }

    @Override
    public void onDestroy(){
        finalize();
        micServiceRunning = false;
        Toast.makeText(this, "Mic service stopping", Toast.LENGTH_SHORT).show();
        getCurrentStatus().setValue(MicStatus.NONE);
    }

    static {
        System.loadLibrary("mic");
        System.loadLibrary("gstreamer_android");
    }

}
