package org.mystic.channeler;

import android.app.NotificationChannel;
import android.app.NotificationManager;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;

import androidx.annotation.RequiresApi;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;


public class MainActivity extends AppCompatActivity {

    // TODO: Change buttons to image buttons and they should respond to state of pipeline using button colours.
    // Requesting permission to RECORD_AUDIO


    @RequiresApi(api = Build.VERSION_CODES.O)
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        createNotificationChannel();

        setContentView(R.layout.activity_main);
        Toolbar myToolbar = findViewById(R.id.toolbar);
        setSupportActionBar(myToolbar);


        // Adding speaker fragment
        /*FragmentManager fragmentManager = getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();
        SpeakerFragment spkFragment = new SpeakerFragment();
        fragmentTransaction.add(R.id.btnGrp1, spkFragment,"speaker");
        MicFragment micFragment = new MicFragment();
        fragmentTransaction.add(R.id.btnGrp1, micFragment,"mic");
        fragmentTransaction.commit();*/


    }

    private void createNotificationChannel() {
        // Create the NotificationChannel, but only on API 26+ because
        // the NotificationChannel class is new and not in the support library
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            CharSequence name = getString(R.string.channel_name);
            String description = getString(R.string.channel_description);
            int importance = NotificationManager.IMPORTANCE_DEFAULT;
            NotificationChannel channel = new NotificationChannel("channeler-notify", name, importance);
            channel.setDescription(description);
            // Register the channel with the system; you can't change the importance
            // or other notification behaviors after this
            NotificationManager notificationManager = getSystemService(NotificationManager.class);
            notificationManager.createNotificationChannel(channel);
        }
    }
    // Method to find out phone's ip address



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
         //Inflate the menu; this adds items to the action bar if it is present.
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return super.onCreateOptionsMenu(menu);
     }

    protected void onSaveInstanceState (Bundle outState) {
        super.onSaveInstanceState(outState);
    }


    protected void onDestroy() {
        super.onDestroy();
    }

    public void goToSettings (MenuItem item){
        Intent settings = new Intent(this, SettingsActivity.class);
        startActivity(settings);
    }

}
