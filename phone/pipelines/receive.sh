AUDIO_CAPS="application/x-rtp,media=(string)audio,clock-rate=(int)48000,encoding-name=(string)X-GST-OPUS-DRAFT-SPITTKA-00"
udpsrc caps=$AUDIO_CAPS port=5000 ! gstrtpjitterbuffer
latency=200 ! rtpopusdepay ! opusdec plc=true ! alsasink
